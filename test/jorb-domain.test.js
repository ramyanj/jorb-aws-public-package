import { DomainObject } from 'jorb-domain'
import * as log from 'jorb-log'
//eslint-disable-next-line
import { setup, server, teardown, expectAndDone, expectOrDone } from './jorb-test'

const fakeResource = 'fake'
class FakeDomain extends DomainObject {
  constructor() {
    super(fakeResource, { ignoreOnSave: ['fakeIgnore'] })
  }

  static resources() {
    return fakeResource
  }
}

beforeEach(() => {
  setup()
})

afterEach(() => {
  teardown()
})

test('DomainObject.get', (done) => {
  log.enableLogging()
  server.respondWith('get', '/api/fake/123', '{ "success": true, "object": { "id": 123 } }')
  FakeDomain.get(123, (obj) => {
    expectOrDone(done, () => {
      expect(obj).not.toBeNull()
      expect(obj instanceof FakeDomain).toBe(true)
      expect(obj.options.ignoreOnSave).toContain('fakeIgnore')
      expect(obj.options.ignoreOnSave).toContain('isLoaded')
      expect(obj.id()).toBe(123)
    })
    FakeDomain.get(123, (obj2) => {
      expectAndDone(done, () => {
        expect(obj2).not.toBeNull()
        expect(obj2 instanceof FakeDomain).toBe(true)
        expect(obj2.options.ignoreOnSave).toContain('fakeIgnore')
        expect(obj2.options.ignoreOnSave).toContain('isLoaded')
        expect(obj2.id()).toBe(123)
      })
    })
  })
})
