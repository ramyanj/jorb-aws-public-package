#!/usr/bin/env groovy
import groovy.io.FileType
import groovy.json.JsonSlurper

/*
 * This script is used to merge multiple less files into a single (or at least fewer) less files.
 *
 * It is used like this...
 *
 * 		groovy less-combine.groovy my-config.json
 *
 *
 * ... where my-config.json looks like this ...
 *
 * 		{
 *			"entries": [
 *				{
 *					"output": "dist/less/my.less",
 *					"append": false,
 *					"files": [
 *						"src/less/my-variables.less",
 *						"src/less/my-mixins.less",
 *						"src/less/my-style.less"
 *					]
 *				},
 *				{
 *					"output": "dist/less/jorb.less",
 *					"append": true,
 *					"wrap-selector": ".jorb",
 *					"files": [
 *						"src/less/jorb.less"
 *					]
 *				}
 *			]
 *		}
 *
 *
 * NOTE: For JORB, we are currently wrapping all CSS in a .jorb wrapper class.  This can be done automatically
 * by using the wrap-selector attribute, as shown above.  Notice how this is in a different "entry" so it only
 * wrals the jorb.less in the wrapper selector, separate from my-variables.less, my-mixins.less, and my-style.less.
 *
 */
class LessCombine
{
  String IMPORT = '@import'
  String EXTEND = ':extend('
  Map config

  public LessCombine(Map config)
  {
    this.config = config
  }
  def combine()
  {
    File root = new File('');
    config.entries.each { entry ->

      File fout = new File(entry.output)
      def prefix = ''

      if(!entry.append)
      {
        fout.delete()
      }


      if(entry['wrap-selector'])
      {
        prefix = '\t'
        fout << entry['wrap-selector'] << ' {\r\n'
      }


      // build list of files, and explode directories into recursive list of files
      List files = []
      entry.paths.each { path ->

        File fin = new File(path)

        if(!fin.exists())
        {
          println "skipping missing file ${path}"
        }
        else if(fin.isDirectory())
        {
          fin.eachFileRecurse (FileType.FILES) { f ->
            files << f
          }
        }
        else
        {
          files << fin
        }
      }

      files.each { fin ->

        println "processing ${fin.absolutePath.replace(root.absolutePath, '')} -> ${fout.absolutePath.replace(root.absolutePath, '')}"

        FileReader f = new FileReader(fin)

        f.each { srcLine ->
          def line = srcLine
          // fix extend lines
          if (line.indexOf(EXTEND) != -1) {
            print "Turned $line "
            line = line.replaceAll(EXTEND.replace('(', '\\(').replace(':', '\\:'), "${EXTEND}.jorb ")
            println "into ${line}"
          }
          // suppress @import lines
          if(line.indexOf(IMPORT)==-1)
          {
            fout << prefix << line << '\r\n'
          }

        }
      }
      if(entry['wrap-selector'])
      {
        fout << '\r\n}'
      }
    }
  }
}

// load config file
String configFileName = args[0]?:'less-combine.json'
println "Running less-combine with config file: ${configFileName}"


File configFile = new File(configFileName)
if(!configFile.exists())
{
  println "Config file not found: ${configFile.absolutePath}"
  return 0
}
JsonSlurper jsonSlurper = new JsonSlurper()
Map config = jsonSlurper.parseText(configFile.text)


new LessCombine(config).combine();

