/**
 * provides client-side routing to support a SPA (single page app) architecture
 */
import * as log from './jorb-log'

const listeners = []
let defaultRoute

const subscribe = (callback, scope = window) => {
  if (typeof callback !== 'function') {
    throw `router.subscribe expects a function but received ${typeof callback}`
  }

  log.debug('listener subscribed to jorb.router')
  listeners.push({ callback, scope })
}

const runRoute = (p) => {
  let params = p
  if (!Array.isArray(params)) {
    params = [params]
  }

  if (log.debug()) {
    log.debug('running route with params: ')
  }
  for (let i = 0; i < listeners.length; i += 1) {
    const listener = listeners[i]
    listener.callback.call(listener.scope, params)
  }
}

const $hashChange = () => {
  try {
    let params = []

    if (window.location.hash.indexOf('#') !== -1) {
      // split fragment (e.g. #/foo/bar/id ) into params that can be passed into handler
      params = window.location.hash.split('#')[1].split('/')
    } else if (defaultRoute != null) {
      // no route, so get the default route from the spa view model
      params = defaultRoute.split('/')
    }

    if (params.length > 0) {
      if (log.loggingEnabled) {
        log.debug(`running route: ${params.join('/')}`)
      }

      runRoute(params)
    } else {
      throw 'Hash is empty; no route found.'
    }
  } catch (exc) {
    // map exception to a 404 route
    log.warn(exc)
    runRoute('404')
  }
}

const init = (config) => {
  log.info('initializing jorb.router')

  if (typeof config.callback === 'function') {
    subscribe(config.callback)
  }

  // setup client-side routing based on hash change
  $(window).on('hashchange', $hashChange)
  $hashChange()
}

const getNavigateAwayMessage = () => 'Nav Away!'

export { init, listeners, subscribe, getNavigateAwayMessage }
