class DomainCache {
  constructor() {
    this.internalMap = {}
  }

  get(type, id) {
    if (!this.internalMap[type]) {
      this.internalMap[type] = {}
    }

    if (!this.internalMap[type][id]) {
      // eslint-disable-next-line new-cap
      this.internalMap[type][id] = new type()
    }

    return this.internalMap[type][id]
  }

  evict(type, id) {
    if (!this.internalMap[type]) {
      return
    }

    if (!this.internalMap[type][id]) {
      return
    }

    delete this.internalMap[type][id]
  }
}

export default DomainCache
